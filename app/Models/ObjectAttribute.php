<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ObjectAttribute extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function contentAttributes(): HasMany
    {
        return $this->hasMany(ContentAttribute::class, 'object_attribute_id');
    }
}
