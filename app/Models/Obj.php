<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Obj extends Model
{
    use HasFactory;

    protected $table = 'objects';
    protected $guarded = [];

    public function attributes(): HasMany
    {
        return $this->hasMany(ObjectAttribute::class, 'object_id');
    }

    public function contents(): HasMany
    {
        return $this->hasMany(Content::class, 'object_id');
    }
}
