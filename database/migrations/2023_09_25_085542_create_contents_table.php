<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('base_id')->nullable();
            $table->foreignId('object_id')->constrained('objects', 'id')->onUpdate('cascade')->onDelete('cascade');
            $table->string('name');
            $table->string('en_name')->nullable();
            $table->integer('quality');
            $table->integer('confidentiality_level');
            $table->integer('version')->default(1);
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->string('created_type')->nullable();
            $table->boolean('deleted')->default(false);
            $table->string('slug');
            $table->boolean('is_active')->default(false);
            $table->boolean('is_draft')->default(true);
            $table->boolean('is_approved')->default(false);
            $table->boolean('is_lock')->default(false);
            $table->boolean('is_last')->default(true);
            $table->string('status')->default('new');
            $table->string('reason_versioning')->nullable();
            $table->string('note')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users', 'id')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('approved_by')->nullable()->constrained('users', 'id')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('edited_by')->nullable()->constrained('users', 'id')->onUpdate('no action')->onDelete('no action');
            $table->foreignId('deleted_by')->nullable()->constrained('users', 'id')->onUpdate('no action')->onDelete('no action');
            $table->timestamp('publish_date')->nullable();
            $table->timestamp('approve_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contents');
    }
};
