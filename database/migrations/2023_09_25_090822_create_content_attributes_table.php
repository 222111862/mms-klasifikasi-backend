<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('content_attributes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('content_id')->constrained('contents', 'id');
            $table->foreignId('object_attribute_id')->constrained('object_attributes', 'id');
            $table->string('value');
            $table->string('language')->nullable();
            $table->foreignId('relation_content_id')->nullable()->constrained('contents', 'id');
            $table->boolean('active')->default(false);
            $table->timestamps();
            $table->foreignId('created_by')->nullable()->constrained('users', 'id')->onUpdate('no action')->onDelete('no action');
            $table->boolean('deleted')->default(false);
            $table->string('en_value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('content_attributes');
    }
};
