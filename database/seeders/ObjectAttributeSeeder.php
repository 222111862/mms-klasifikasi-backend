<?php

namespace Database\Seeders;

use App\Models\ObjectAttribute;
use App\Models\Obj;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ObjectAttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::unprepared('SET IDENTITY_INSERT object_attributes ON');

        $classificationFamily = Obj::where('slug', 'classification-family')->first();
        if ($classificationFamily === null) {
            return;
        }

        ObjectAttribute::insert([
            [
                'id' => 11,
                'slug' => 'classification-family-name',
                'object_id' => 3,
                'order' => 1,
                'name' => 'nama',
                'en_name' => 'name',
                'cardinality' => 2,
                'required' => true,
                'value_type' => 2,
                'active' => true
            ],
            [
                'id' => 12,
                'slug' => 'classification-family-description',
                'object_id' => 3,
                'order' => 2,
                'name' => 'deskripsi',
                'en_name' => 'description',
                'cardinality' => 0,
                'required' => false,
                'value_type' => 3,
                'active' => true
            ],
            [
                'id' => 24,
                'slug' => 'classification-series-abbreviation',
                'object_id' => 6,
                'order' => 1,
                'name' => 'singkatan',
                'en_name' => 'abbreviation',
                'cardinality' => 0,
                'required' => false,
                'value_type' => 2,
                'active' => true
            ],
            [
                'id' => 25,
                'slug' => 'classification-series-name',
                'object_id' => 6,
                'order' => 2,
                'name' => 'nama',
                'en_name' => 'name',
                'cardinality' => 2,
                'required' => true,
                'value_type' => 2,
                'active' => true
            ]
        ]);

        DB::unprepared('SET IDENTITY_INSERT object_attributes OFF');
    }
}
