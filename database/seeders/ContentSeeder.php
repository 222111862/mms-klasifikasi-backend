<?php

namespace Database\Seeders;

use App\Models\Content;
use App\Models\Obj;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $classificationSeries = Obj::where('slug', 'classification-series')->first();
        if ($classificationSeries == null) return;

        $classificationSeries->contents()->create([
            'name' => 'Kode dan Wilayah Kerja Statistik',
            'quality' => 100,
            'confidentiality_level' => 0,
            'version' => 1,
            'active' => true,
            'slug' => 'da92ea88-bc22-4030-963e-cad3efb7c96b'
        ]);
    }
}
