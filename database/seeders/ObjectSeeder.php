<?php

namespace Database\Seeders;

use App\Models\Obj;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ObjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::unprepared('SET IDENTITY_INSERT objects ON');

        Obj::insert([
            [
                'id' => 3,
                'slug' => 'classification-family',
                'name' => 'classification family',
                'en_name' => 'classification family',
                'description' => 'Classification family adalah suatu grup yang digunakan untuk mengelompokkan klasifikasi-klasifikasi berdasarkan sudut pandang tertentu. Classification familiy biasanya didasarkan oleh suatu konsep umum (mis. Kegiatan ekonomi).',
                'en_description' => 'A Classification Family is a group of Classification Series related from a particular point of view. The Classification Family is related by being based on a common Concept (e.g. economic activity).',
                'confidentiality_level' => 0,
                'versionable' => true
            ],
            [
                'id' => 4,
                'slug' => 'classification-item',
                'name' => 'classification item',
                'en_name' => 'classification item',
                'description' => 'Classification items merepresenatsikan Kategori pada Tingkat tertentu dalam Klasifikasi Statistik serta mendefinisikan konten dan perbatasan dari Kategori tersebut. Item Klasifikasi menggabungkan makna dari Kategori, kode yang mereprenstasikannya dan informasi tambahan untuk memenuhi kriteria Klasifikasi Statistik, misalnya "Pertanian, kehutanan dan perikanan" dan teks penjelasan yang menyertainya seperti informasi tentang apa yang termasuk dan pengecualian.',
                'en_description' => 'Classification items represent Categories at a certain Level in a Statistical Classification. It defines content and borders Category. A Classification Item combines the meaning from a Category, its representation (i.e., Code) and additional information in order to meet the Statistical Classification criteria, for example "Agriculture, forestry and fishing" and accompanying explanatory text such as information about what is included and excluded.',
                'confidentiality_level' => 0,
                'versionable' => true
            ],
            [
                'id' => 6,
                'slug' => 'classification-series',
                'name' => 'classification series',
                'en_name' => 'classification series',
                'description' => 'Classfication series adalah pengelompokkan dari satu atau lebih Klasifikasi Statistik, berdasarkan konsep yang sama, dan memiliki keterkaitan satu sama lain sebagai versi atau pembaruan. Biasanya, Klasifikasi Statistik ini memiliki nama yang sama (misalnya, KBLI, KBKI, ISIC atau ISCO)',
                'en_description' => 'A Classification Series is an ensemble of one or more Statistical Classifications, based on the same concept, and related to each other as versions or updates. Typically, these Statistical Classifications have the same name (for example, KBLI, KBKI, ISIC or ISCO)',
                'confidentiality_level' => 0,
                'versionable' => true
            ]
        ]);

        Obj::insert([
            [
                'id' => 5,
                'slug' => 'classification-item-history',
                'name' => 'classification item history',
                'en_name' => 'classification item history',
                'confidentiality_level' => 0,
                'versionable' => true
            ],
        ]);

        DB::unprepared('SET IDENTITY_INSERT objects OFF');
    }
}
