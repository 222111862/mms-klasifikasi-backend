<?php

namespace Database\Seeders;

use App\Models\Content;
use App\Models\ContentAttribute;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ContentAttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $content = Content::first();

        ContentAttribute::insert([
            'content_id' => $content->id,
            'object_attribute_id' => 11,
            'value' => 'tes',
            'active' => true
        ]);
    }
}
